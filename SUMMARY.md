# Summary

## Introduction

* [Introduction](README.md)
* [Charts](charts.md)

## Charts

* [Dynamic Trend Chart](dynamic-trend-chart.md)
* [Bump Chart](bump-chart.md)
* [Waterfall Chart](waterfall-chart.md)
* [Donut Chart](donut-chart.md)
* [Pareto Chart](pareto-chart.md)



