# Bump Chart

Now we have a Line Chart which describes the Sales Trend by Segment as below, then the Bump Chart, also known as Rank Chart can be used for cross validation when the clear changing process is needed.
![](./viz/bump-chart/trend-chart.png)

* Step1: Drag the **Order Date** \(take by month as demonstration\) into **Columns** and drag **Segment** into **Color** in the Marks card. Then create a calculated field **Ranking** for sales rank along the month and drag it into **Rows** twice. Now the two ranking charts just look like normal line charts. ![](./viz/bump-chart/step1.png)

* Step2: Right click both of **Ranking** in **Rows** and choose **Compute using** => **Segment**. Now choose **Line** chart type for first **Ranking** field and **Circle** for second **Ranking** field. Then make **Rows** into **Dual Axis**. ![](./viz/bump-chart/step2.png)

* Step3: Right click the axis and choose **Edit Axis** and make **Reversed** checked, so the Top 1 will be at first row. You may set the **Text** into whatever you like \(**Percent of Total** in this example\).  ![](./viz/bump-chart/step3.png)

PS. Put the trend chart and bump chart together into a dashboard so that it is clear to see the fluctuation.  ![](./viz/bump-chart/step4.png)




