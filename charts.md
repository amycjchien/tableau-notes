# Charts

* [Dynamic Trend Chart](/dynamic-trend-chart.md)
* [Bump Chart \(Rank Chart\)](/bump-chart.md)
* [Waterfall Chart](/waterfall-chart.md)
* [Donut Chart](/donut-chart.md)
* [Pareto Chart](/pareto-chart.md)




