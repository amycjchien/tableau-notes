# Dynamic Trend Chart

* Step1: Put **Order Date** \(take by month as demonstration\) into **Columns** and put **Sales** and **Profits** into **Rows**. Make **Rows** into **Dual Axis**.![](/viz/dynamic-trend-chart/step1.png)

* Step2: Make **Marks** from Line into **Circle**. Put **Order Date** into Pages and there will be a card shown in right-hand side of the screen. Make the **Show history** checked and choose **All** for **Marks to show history for** and choose **Both** for **Show**. ![](/viz/dynamic-trend-chart/step2.png)

* Step3: Now your dynamic trend chart is ready to go! Only works for Tableau Desktop but you can make it into GIF animation.


