# Waterfall Chart

Waterfall Chart is to visualize the cumulative effect of sequentially introduced positive or negative values. Let's transform the Profit Trend Chart into Waterfall Chart as an example. ![](./viz/waterfall-chart/trend-chart.png)

* Step1: Just follow the steps we built the profit trend chart. Drag the **Order Date** \(take by month as demonstration\) into Columns and **Profit** into **Rows**. Right click the **Profit** and select **Quick Table Calculation** => **Running Total**. Then there will be a line chart which shows the cumulative summation of profits. ![](./viz/waterfall-chart/running-total.png)

* Step2: Change the **Mark Type** from automatic into **Gantt Bar**. ![](./viz/waterfall-chart/gantt.png)

* Step3: Create a calculated field **Neg Profit** and drag into **Size** so that it can present the amount of profit at the specific moment. And drag **Profit** into **Color**. ![](./viz/waterfall-chart/neg-profit.png)

* Step4: In order to be eaiser to see the negative and positive difference, click **Color** => **Edit Colors...** and make the palette into two-step color. ![](./viz/waterfall-chart/step-color.png)

Then the Waterfall Chart is done.  ![](./viz/waterfall-chart/waterfall-chart.png)






